<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context encoding="UTF-8">
    <name>Dialog</name>
    <message encoding="UTF-8">
        <location filename="../windows/requirement_dialog.py" line="60"/>
        <source>Nastavení omezení</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/requirement_dialog.py" line="62"/>
        <source>nesmí</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/requirement_dialog.py" line="63"/>
        <source>musí</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/requirement_dialog.py" line="64"/>
        <source>být v týmu s:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>Form</name>
    <message>
        <location filename="../windows/table.py" line="46"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/groupsoverview.py" line="35"/>
        <source>Vytvořit skupinu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/groupedit.py" line="130"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/groupedit.py" line="131"/>
        <source>Rozdělit</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/groupedit.py" line="132"/>
        <source>Počet členů:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/groupedit.py" line="133"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/groupedit.py" line="134"/>
        <source>Členové</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/groupedit.py" line="135"/>
        <source>Historie</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/groupedit.py" line="136"/>
        <source>Jméno</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/groupedit.py" line="137"/>
        <source>Přidat</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="148"/>
        <source>Rozdělení skupiny</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/distributionsetup.py" line="149"/>
        <source>GroupName</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="150"/>
        <source>(x členů)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="151"/>
        <source>Počet týmů:</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="152"/>
        <source>Minimální velikost týmu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="153"/>
        <source>Maximální velikost týmu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/distributionsetup.py" line="154"/>
        <source>Preferovat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/distributionsetup.py" line="155"/>
        <source>Nic</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="156"/>
        <source>Stejné týmy</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="157"/>
        <source>Nové týmy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/distributionsetup.py" line="160"/>
        <source>ignorovat</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distributionsetup.py" line="161"/>
        <source>Omezení:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/distributionsetup.py" line="162"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distribution_overview.py" line="73"/>
        <source>Přidat tým</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/distribution_overview.py" line="74"/>
        <source>Tisk</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/distribution_overview.py" line="75"/>
        <source>Uložit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/member_overview.py" line="55"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/print_widget.py" line="43"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/editable_title.py" line="47"/>
        <source>edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/qt/distribution_setup.ui" line="27"/>
        <source>Rozdělení skupiny </source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../windows/qt/distribution_setup.ui" line="88"/>
        <source>Maxmální velikost týmu:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../windows/mainwindow.py" line="52"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windows/mainwindow.py" line="53"/>
        <source>back</source>
        <translation>Zpět</translation>
    </message>
</context>
<context>
    <name>MemberOverview</name>
    <message>
        <location filename="../controllers/member_overview.py" line="38"/>
        <source>Ve skupin&#xc4;&#x9b; {0} ji&#xc5;&#xbe; existuje &#xc4;&#x8d;len s t&#xc3;&#xad;mto jm&#xc3;&#xa9;nem.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
