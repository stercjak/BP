#!/usr/bin/env python3
import os
import sqlite3
import sys
from argparse import ArgumentParser
import utils.database_create as database_create
from PyQt5 import QtWidgets as Qt
from PyQt5.QtCore import QTranslator
from controllers.mainwindow_controller import MainWindow
from utils.database import Database
from utils.globals import Global
from resources import resources_rc


def main():
    script_path = os.path.dirname(os.path.abspath(__file__))
    parser = ArgumentParser()
    parser.add_argument(
        '-d',
        '--database',
        default=script_path + '/database.db',
        help='use specified file as database file, if not specified the default file (script_path/database.db} is used',
        metavar='PATH'
    )
    parser.add_argument(
        '-fc',
        '--force-database-create',
        action='store_true',
        help='force creation of database - program then overwrites file specified in -d or the default one!'
    )
    args = parser.parse_args()
    # clear and create database if -fc was given or create database if given file doesn't exist
    if args.force_database_create is True or not os.path.isfile(args.database):
        if os.path.isfile(args.database):
            overwrite = input("Are you sure, you want to overwrite file '" + args.database + "'?[Y/N]\n")
            if overwrite.upper() != 'Y':
                exit()
            os.remove(args.database)
        database_create.create_db(args.database)

    try:
        Global.db = Database(args.database)
    except (sqlite3.DatabaseError, sqlite3.OperationalError) as e:
        print('The specified database file is invalid.\nTry rerunning with -fc?')
        exit()
    app = Qt.QApplication(sys.argv)
    translator = QTranslator()
    translator.load(script_path + "translations/czech.qm")
    app.installTranslator(translator)
    form = MainWindow()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
