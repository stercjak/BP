from random import shuffle

import time

from model.requirements import RequirementSameTeam, RequirementDifferentTeam


class DistributionMaker:
    PREFER_SAME_TEAMS = False
    PREFER_DIFFERENT_TEAMS = True

    def __init__(self, members, requirements, enum_violated, team_count, min_team_size, max_team_size, prefer):
        """
        class for finding distribution
        :param members: list of members to be distributed
        :param requirements: list of requirements for the distribution
        :param violated_enum: enum for values to put in violated requirements instead of:
                team_count, min_team_size and max_team_size
        :param team_count: int for how many teams should there be in distribution, None if not specified 
        :param min_team_size: int for what is the minimum size of team in distribution, None if not specified
        :param max_team_size: int for what is the maximum size of team in distribution, None if not specified
        :param prefer: DistributionMaker.PREFER_SAME_TEAMS, DistributionMaker.PREFER_DIFFERENT_TEAMS or None
        """
        self.requirements_same = [
            requirement for requirement in requirements if isinstance(requirement, RequirementSameTeam)
        ]
        self.requirements_different = [
            requirement for requirement in requirements if isinstance(requirement, RequirementDifferentTeam)
        ]
        self.team_count = team_count
        self.member_count = len(members)
        self.req_min_team_size = self.min_team_size = min_team_size if min_team_size is not None else 1
        self.req_max_team_size = self.max_team_size = max_team_size if max_team_size is not None else self.member_count
        self.prefer = prefer
        self.members = members
        self.unassigned = [{member} for member in members]
        self.violated_requirements = []
        self.enum_violated = enum_violated
        self.initMemberBlackList(self.requirements_different)

    def initMemberBlackList(self, requirements):
        """
        initialize 'blacklist' for each member (list of members he can't be in team with)
        :param requirements: 
        """
        for member in self.members:
            member.blacklist_ = set()
        for requirement in requirements:
            new_member_blacklist = requirement.member.blacklist_ | set(requirement.target_members)
            if len(new_member_blacklist) > len(self.members) - (self.min_team_size):
                # we wouldn't be able to create a team with this member, that's at least min_team_size large
                self.violated_requirements.append(requirement)
                break
            requirement.member.blacklist_ = new_member_blacklist
            for member in requirement.target_members:
                new_blacklist = member.blacklist_ | {requirement.member}
                if len(new_blacklist) > len(self.members) - (self.min_team_size - 1):
                    # we wouldn't be able to create a team with this member, that's at least min_team_size large
                    self.violated_requirements.append(requirement)
                    break
                member.blacklist_ = new_blacklist

    def distribute(self):
        """
        create distribution
        THIS SHOULD ONLY BE CALLED ONCE!
        :return: map of teams (consisting of members) - {"team1": {Member1, Member2,...}, "team2": {Member, ...}}
        """
        self.checkTeamSizeLimits()

        shuffle(self.unassigned)
        self._makeTeamsFromRequirements()
        teams = self._solve()
        for team in teams:
            if len(team) < self.req_min_team_size:
                if self.enum_violated.MIN_TEAM_SIZE not in self.violated_requirements:
                    self.violated_requirements.append(self.enum_violated.MIN_TEAM_SIZE)
            if len(team) > self.req_max_team_size:
                if self.enum_violated.MAX_TEAM_SIZE not in self.violated_requirements:
                    self.violated_requirements.append(self.enum_violated.MAX_TEAM_SIZE)
            self.checkTeamValid(team)
        if len(teams) != self.team_count:
            self.violated_requirements.append(self.enum_violated.TEAM_COUNT)
        return teams

    def getViolatedRequirements(self):
        """
        :return: requirements that are violated by the distribution created in distribute
         only makes sense to call after calling distribute()
        """
        return self.violated_requirements

    def checkTeamSizeLimits(self):
        """
        check whether the team size limits (max_team_size, min_team_size, team_count) are satisfiable
        if not, change them to (basically) unset
        """
        if self.max_team_size < self.min_team_size:
            self.max_team_size = self.member_count
            self.min_team_size = 1

        if self.team_count is not None:
            avg_team_size = self.member_count / self.team_count
            if self.min_team_size > avg_team_size:
                self.min_team_size = 1
            if self.max_team_size < avg_team_size:
                self.max_team_size = self.member_count

    def checkTeamValid(self, team):
        """
        checks if the team is not longer than max_team_size and doesn't violate any requirement
        if it is longer/violates requirement it adds those to self.violated_requirements
        WARNING: the team can still be smaller than min_team_size
        :param team: the checked team
        """
        if len(team) > self.max_team_size:
            self.violated_requirements.append(self.enum_violated.MAX_TEAM_SIZE)
        for member in team:
            violated = []
            for member_blacklist in member.blacklist_:
                if member_blacklist in team:
                    violated.append(member_blacklist)
            if len(violated) > 0:
                self.violated_requirements.append(RequirementDifferentTeam(member, violated))

    def _makeTeamsFromRequirements(self):
        """
        make teams (sets) from members in self.unassigned based on requirements of 'must be in the same team' 
        """
        teams = []
        for requirement in self.requirements_same:
            team = set(requirement.target_members + [requirement.member])
            team_nr = None
            for member in team:
                # remove member from unassigned
                try:
                    self.unassigned.remove({member})
                except ValueError:
                    pass
                # find if the member is already in some team
                for i, t in enumerate(teams):
                    if member in t:
                        if team_nr is None:
                            # set the team_nr we want to append these members to
                            team_nr = i
                        elif team_nr != i:
                            # some member is in a team and this one is in another team -
                            # we have to merge those teams
                            result_team_nr = min([i, team_nr])
                            teams[result_team_nr] = teams[i] | teams[team_nr]
                            del teams[max(i, team_nr)]
                            team_nr = result_team_nr
            if team_nr is not None:
                teams[team_nr] |= team
            else:
                teams.append(team)

        for team in teams:
            # check team validity
            self.checkTeamValid(team)
        # add created teams to unassigned
        self.unassigned += teams

    def canAddToTeam(self, who, team):
        """
        check if member(s) can be added to team without violating max_team_size or any requirements
        :param who: members that are being added (iterable)
        :param team: (iterable)
        :return: True if can be added, False otherwise 
        """
        if len(team) + len(who) > self.max_team_size:
            return False
        for member in who:
            for member_blacklist in member.blacklist_:
                if member_blacklist in team:
                    return False
        return True

    def _solve(self):
        """
        find required distribution
        :return: list of teams [{Member1, Member2, ...}, ...]
        """
        teams = []
        team_count = self.team_count if self.team_count is not None else self.member_count // self.max_team_size
        unassigned = list(self.unassigned)
        while len(unassigned) > 0:
            if len(teams) < team_count:
                teams.append(unassigned[0])
                unassigned.remove(unassigned[0])
            else:
                teams.sort(key=len)
                if self.prefer is not None:
                    teams.sort(
                        key=lambda t: sum(member.history_count[member_with] for member_with in t for member in unassigned[0]) / len(t),
                        reverse=self.prefer
                    )
                for i in range(len(teams)):
                    if self.canAddToTeam(unassigned[0], teams[i]):
                        teams[i] |= unassigned[0]
                        unassigned.remove(unassigned[0])
                        break
                else:
                    teams.append(set(unassigned[0]))
                    unassigned.remove(unassigned[0])
        return teams

    def printTeams(self, teams):
        """
        debug function
        """
        for i, team in enumerate(teams):
            print("Team ", i, ":", sep='')
            for member in team:
                print(member.name, end=", ")
            print()
        print()