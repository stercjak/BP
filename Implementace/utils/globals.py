class Global:
    """
    class for things I want to have everywhere (after importing it), so that I don't have to pass them all the time
    """
    db = None
