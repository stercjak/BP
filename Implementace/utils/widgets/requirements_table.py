from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton

from model.requirements import RequirementSameTeam
from utils.qtmodels.groups_table_model import GroupsTableModel
from utils.qtmodels.requirements_table_model import RequirementsTableModel
from utils.widgets.table import ElemTable


class RequirementsTable(ElemTable):
    def __init__(self, num_of_buttons=0, button_definitions=(), data=[], parent=None):
        """
        :param num_of_buttons: number of buttons that should be added to table
        :param button_definitions: iterable of dicts that define the buttons; 
                first dict in structure defines first column buttons in table, second defines second column and so on
                the dictionary has to contain 'text' key with string value of text to be displayed on button,
                    it can also contain 'icon' key with string value of icon location (e.g. ':/icons/eye')
                    it can also contain 'tooltip' key with string value of tooltip to be displayed on mouseover
                the length of structure has to be at least == num_of_buttons, any additional values in list are ignored
        :param data: data to be displayed in table (list of requirements)
        :param parent: parent node for qt 
        """
        super(RequirementsTable, self).__init__(RequirementsTableModel(data, num_of_buttons + 3), num_of_buttons, button_definitions, parent)

    def changeData(self, data):
        """
        change data in table
        :param data: new data for table (list of requirements)
        """
        self.setModel(GroupsTableModel(data, self._btn_amount + 3))

    def insertRequirement(self, requirement):
        """
        add new requirement to table
        :param requirement: requirement to be added
        """
        self.model.setDynamicSortFilter(False)
        self.model.sourceModel().insertRequirement(0, requirement)

        # add buttons to new row
        for btn_nr in range(self._btn_amount):
            btn_def = self._btn_defs[btn_nr]
            button = QPushButton(btn_def['text'])
            if 'icon' in btn_def.keys():
                button.setIcon(QIcon(btn_def['icon']))
            if 'tooltip' in btn_def.keys():
                button.setToolTip(btn_def['tooltip'])
            button.clicked.connect(self.onBtnClicked)
            requirement = self.model.data(self.model.index(0, 0), Qt.UserRole)
            if isinstance(requirement, RequirementSameTeam):
                req_type = "same"
            else:
                req_type = "diff"
            button.setObjectName(str(requirement.member.id_) + "_" + req_type + "_" + str(btn_nr))
            self.table.setIndexWidget(self.model.index(0, btn_nr+self._data_columns), button)

        self.model.setDynamicSortFilter(True)

    def onBtnClicked(self):
        req_person, req_type, btn_id = str.split(self.sender().objectName(), '_')
        if req_type == "same":
            is_same_team = True
        elif req_type == "diff":
            is_same_team = False
        requirement = self.model.sourceModel().getRequirement(req_person, is_same_team)
        self.btnClicked.emit(requirement, int(btn_id))

    def getAllRequirements(self):
        """
        :return: list of all requirements in table 
        """
        return self.source_model.requirements
