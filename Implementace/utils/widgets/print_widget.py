from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QFrame

from utils.qtmodels.members_table_model import MembersTableModel
from utils.widgets.table import ElemTable
from windows import print_widget


class PrintWidget(QWidget, print_widget.Ui_Form):
    """
    Helper widget that transforms MemberTeamTable()s to printable layout
    """
    def __init__(self, label_text, team_tables, parent=None):
        """
        :param label_text: text of label that should be displayed above all tables
        :param team_tables: list of MemberTeamTable() 
        :param parent: Qt parent
        """
        super(PrintWidget, self).__init__(parent)
        self.setupUi(self)
        self.lblDistName.setText(label_text)
        self.tables = []
        max_rows = max([table.model.rowCount() for table in team_tables])
        height = PrintTable.ROW_HEIGHT * max_rows + 50
        for i, team in enumerate(team_tables):
            table = PrintTable(team.getAllData(), team.name, self)
            table.setMinimumHeight(height)
            self.gridTables.addWidget(table, i // 2, i % 2)


class PrintTable(ElemTable):
    ROW_HEIGHT = 30

    def __init__(self, data, label_text, parent):
        """
        print optimized table for displaying members 
        :param data: list of members
        :param label_text: text to be displayed above the table (team name)
        :param parent: Qt parent
        """
        super(PrintTable, self).__init__(MembersTableModel(data, 2), 0, [], parent)

        layout = QHBoxLayout()
        self.lblName = QLabel(label_text)
        layout.addWidget(self.lblName)
        self.layout.insertLayout(0, layout)
        self.table.horizontalHeader().setVisible(False)
        self.table.setShowGrid(True)
        self.table.setFrameStyle(QFrame.Box)
        self.table.setFrameShadow(QFrame.Plain)
        self.table.setLineWidth(2)
        self.table.setMidLineWidth(0)

