from PyQt5.QtGui import QIcon
from utils.qtmodels.custom_filter_model import CustomFilterModel
from windows import table
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class ElemTable(QWidget, table.Ui_Form):
    _ICON_WIDTH = 30

    btnClicked = pyqtSignal(object, int)
    elementSelected = pyqtSignal(object)
    itemDoubleClicked = pyqtSignal(object)

    def __init__(self, source_model, num_of_buttons, button_definitions, parent):
        """
        :param source_model: underlying model of table 
        :param num_of_buttons: number of buttons that should be added to table
        :param button_definitions: iterable of dicts that define the buttons; 
                first dict in structure defines first column buttons in table, second defines second column and so on
                the dictionary has to contain 'text' key with string value of text to be displayed on button,
                    it can also contain 'icon' key with string value of icon location (e.g. ':/icons/eye')
                    it can also contain 'tooltip' key with string value of tooltip to be displayed on mouseover
                the length of structure has to be at least == num_of_buttons, any additional values in list are ignored
        :param parent: parent node for qt
        """
        super(ElemTable, self).__init__(parent)
        self.setupUi(self)

        self._btn_defs = button_definitions
        self._btn_amount = num_of_buttons
        self._data_columns = source_model.columnCount() - num_of_buttons

        self.source_model = source_model
        self.model = CustomFilterModel()
        self.model.setSourceModel(source_model)
        self.table.setModel(self.model)
        self.insertButtons()

        # initialize table settings (and signals)
        header_view = self.table.horizontalHeader()
        for i in range(self._data_columns):
            header_view.setSectionResizeMode(i, QHeaderView.Stretch)
        for i in range(self._btn_amount):
            header_view.setSectionResizeMode(i+self._data_columns, QHeaderView.Fixed)
            section_width = len(self._btn_defs[i]['text']) * 5
            if 'icon' in self._btn_defs[i].keys():
                section_width += self._ICON_WIDTH
            header_view.resizeSection(i + self._data_columns, section_width)
        self.table.clicked.connect(self.onElementSelected)
        self.table.activated.connect(self.onElementSelected)
        self.table.selectionModel().currentChanged.connect(self.onElementSelected)
        self.table.doubleClicked.connect(self.onDoubleClicked)

        self.table.sortByColumn(0, Qt.AscendingOrder)

    def insertButtons(self):
        """
        correctly name and insert buttons to table
        """
        for btn_nr in range(self._btn_amount):
            for i in range(self.model.rowCount()):
                btn_def = self._btn_defs[btn_nr]
                button = QPushButton(btn_def['text'])
                if 'icon' in btn_def.keys():
                    button.setIcon(QIcon(btn_def['icon']))
                if 'tooltip' in btn_def.keys():
                    button.setToolTip(btn_def['tooltip'])
                button.clicked.connect(self.onBtnClicked)
                item = self.model.data(self.model.index(i, 0), Qt.UserRole)
                button.setObjectName(str(item.id_) + "_" + str(btn_nr))
                self.table.setIndexWidget(self.model.index(i, btn_nr+self._data_columns), button)

    @pyqtSlot(QModelIndex)
    def onDoubleClicked(self, index):
        """
        slot to be called when item is double clicked
        :param index: QModelIndex of row that has been double clicked
        """
        self.itemDoubleClicked.emit(self.model.data(index, Qt.UserRole))

    @pyqtSlot()
    def onBtnClicked(self):
        """
        slot to be called when button in table is clicked
        """
        elem_id, btn_id = str.split(self.sender().objectName(), '_')
        elem = self.source_model.getElementById(elem_id)
        self.btnClicked.emit(elem, int(btn_id))

    @pyqtSlot()
    def onElementSelected(self):
        """
        slot to be called when element is selected
        """
        elem = self.model.data(self.table.currentIndex(), Qt.UserRole)
        self.elementSelected.emit(elem)

    def setModel(self, model):
        """
        set model of table
        :param model: new model for table
        """
        self.model.beginResetModel()
        self.model.setSourceModel(model)
        self.source_model = model
        self.model.endResetModel()
        self.insertButtons()
        self.table.sortByColumn(0, Qt.AscendingOrder)

    def getSelectedItems(self):
        """
        :return: list of selected items from table 
        """
        indexes = self.table.selectionModel().selectedRows()
        res = []
        for index in indexes:
            res.append(self.model.data(index, Qt.UserRole))
        return res
