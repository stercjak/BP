from utils.qtmodels.history_count_table_model import HistoryCountTableModel
from utils.widgets.table import ElemTable


class HistoryCountTable(ElemTable):
    def __init__(self, num_of_buttons=0, button_titles=(), data={}, parent=None):
        """
        :param num_of_buttons: number of buttons that should be added to table
        :param button_definitions: iterable of dicts that define the buttons; 
                first dict in structure defines first column buttons in table, second defines second column and so on
                the dictionary has to contain 'text' key with string value of text to be displayed on button,
                    it can also contain 'icon' key with string value of icon location (e.g. ':/icons/eye')
                    it can also contain 'tooltip' key with string value of tooltip to be displayed on mouseover
                the length of structure has to be at least == num_of_buttons, any additional values in list are ignored
        :param data: data to be displayed in table (dictionary  {Member: int, Member: int, Member: int, ... })
        :param parent: parent node for qt 
        """
        super(HistoryCountTable, self).__init__(HistoryCountTableModel(data, num_of_buttons + 2), num_of_buttons, button_titles, parent)


    def changeData(self, data):
        """
        change data in table
        :param data: new data for table (dictionary  {Member: int, Member: int, Member: int, ... })
        """
        self.setModel(HistoryCountTableModel(data, self._btn_amount + 2))