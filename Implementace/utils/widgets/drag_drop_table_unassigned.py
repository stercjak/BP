from utils.widgets.members_drag_drop_table import MembersDragDropTable


class DragDropTableUnassigned(MembersDragDropTable):
    """
    class for drag drop table that can't be deleted and its name can't be changed
    """
    def __init__(self, id_, data, label_text, parent=None):
        super(DragDropTableUnassigned, self).__init__(id_, data, label_text, parent)
        self.lblName.btnEdit.hide()
        self.btnDelete.hide()
        self.layout.setSpacing(0)
