from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QLineEdit
from windows import editable_title


class EditableTitle(QWidget, editable_title.Ui_Form):
    button_width = 30
    min_length = 1
    max_length = 35
    
    textConfirmed = pyqtSignal(str)

    def __init__(self, text, parent=None):
        """
        widget with title that is editable by user
        :param text: text to be displayed before edit
        :param parent: 
        """
        super(EditableTitle, self).__init__(parent)
        self.setupUi(self)
        self.btnEdit.setFixedWidth(self.button_width)
        self.label.setText(text)
        self.editLine = QLineEdit()
        self.editLine.textChanged.connect(self.onEditChanged)
        self.editLine.returnPressed.connect(self.displayMode)
        self.btnEdit.clicked.connect(self.editMode)
        self.btnEdit.setToolTip(self.tr("Přejmenovat"))

    @pyqtSlot()
    def editMode(self):
        """
        change to edit mode - display line edit and change button to confirmation
        also sets focus to edit
        """
        self.btnEdit.setIcon(QIcon(':/icons/ok.svg'))
        self.label.hide()
        if self.label.width() > 100:
            self.editLine.setFixedWidth(self.label.width())
        else:
            self.editLine.setFixedWidth(100)
        self.editLine.setFixedHeight(self.label.height())

        self.editLine.setText(self.label.text())
        self.layout().replaceWidget(self.label, self.editLine)
        self.editLine.show()

        # focus
        self.editLine.setFocus()
        self.editLine.selectAll()

        self.btnEdit.clicked.disconnect(self.editMode)
        self.btnEdit.clicked.connect(self.displayMode)

        self.btnEdit.setToolTip(self.tr("Musí být v rozmezí ") + str(self.min_length)
                                + self.tr(" až ") + str(self.max_length) + self.tr(" znaků."))

    @pyqtSlot()
    def displayMode(self):
        """
        change to display mode (this is the default mode)
        """
        self.btnEdit.setIcon(QIcon(':/icons/edit.svg'))
        self.editLine.hide()
        self.label.setText(self.editLine.text())
        self.layout().replaceWidget(self.editLine, self.label)
        self.label.show()
        self.btnEdit.clicked.disconnect(self.displayMode)
        self.btnEdit.clicked.connect(self.editMode)
        self.textConfirmed.emit(self.editLine.text().strip())
        self.btnEdit.setToolTip(self.tr("Přejmenovat"))

    @pyqtSlot()
    def onEditChanged(self):
        """
        check if the text in edit fits requirements and disable/enable confirm button
        """
        if self.min_length <= len(self.editLine.text()) <= self.max_length:
            self.btnEdit.setDisabled(False)
        else:
            self.btnEdit.setDisabled(True)

    def text(self):
        """
        :return: current text of label (there can be a different text in edit line currently, if we're in edit mode) 
        """
        return self.label.text().strip()

