from utils.qtmodels.groups_table_model import GroupsTableModel
from utils.widgets.table import ElemTable


class GroupsTable(ElemTable):
    def __init__(self, num_of_buttons=0, button_definitions=(), data=[], parent=None):
        """
        :param num_of_buttons: number of buttons that should be added to table
        :param button_definitions: iterable of dicts that define the buttons; 
                first dict in structure defines first column buttons in table, second defines second column and so on
                the dictionary has to contain 'text' key with string value of text to be displayed on button,
                    it can also contain 'icon' key with string value of icon location (e.g. ':/icons/eye')
                    it can also contain 'tooltip' key with string value of tooltip to be displayed on mouseover
                the length of structure has to be at least == num_of_buttons, any additional values in list are ignored
        :param data: data to be displayed in table (list of groups)
        :param parent: parent node for qt 
        """
        super(GroupsTable, self).__init__(GroupsTableModel(data, num_of_buttons + 1), num_of_buttons, button_definitions, parent)

    def changeData(self, data):
        """
        change data in table
        :param data: new data for table (list of groups)
        """
        self.setModel(GroupsTableModel(data, self._btn_amount + 1))
