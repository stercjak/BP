from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QPoint, QCoreApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAbstractItemView, QHBoxLayout, QPushButton
from utils.widgets.table import ElemTable
from model.member import Member
from utils.qtmodels.members_drag_drop_table_model import MembersDragDropTableModel
from utils.widgets.editable_title_widget import EditableTitle


class MembersDragDropTable(ElemTable):
    # signals that delete was clicked, param: id of table
    deleteClicked = pyqtSignal(int)
    # signals that context menu has been requested
    # params: Member over which the context menu was requested,
    #   id of this table
    #   QPoint of exact location, where the context menu was requested
    contextMenuRequested = pyqtSignal(Member, int, QPoint)

    def __init__(self, id_, data, label_text, parent=None):
        """
        table for displaying members with drag and drop enabled
        :param id_: 
        :param data: 
        :param label_text: 
        :param parent: 
        """
        super(MembersDragDropTable, self).__init__(MembersDragDropTableModel(id_, data, num_columns=1), 0, [], parent)
        self.id_ = id_

        layout = QHBoxLayout()
        self.lblName = EditableTitle(label_text, self)
        layout.addWidget(self.lblName)
        self.btnDelete = QPushButton()
        self.btnDelete.setIcon(QIcon(':/icons/minus.svg'))
        self.btnDelete.setToolTip(QCoreApplication.translate('DistributionOverview', 'Odebrat tým'))
        self.btnDelete.setFixedHeight(30)
        self.btnDelete.clicked.connect(self.onDeleteClicked)
        layout.addStretch()
        layout.addWidget(self.btnDelete)
        self.layout.insertLayout(0, layout)

        self.table.setDragDropMode(QAbstractItemView.DragDrop)
        self.table.setDefaultDropAction(Qt.MoveAction)
        self.table.setAcceptDrops(True)
        self.table.setDragEnabled(True)
        self.table.setDropIndicatorShown(False)
        self.table.setContextMenuPolicy(Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.onCustomContextMenuRequested)

    def removeMember(self, member_id):
        """
        remove member with given id from table
        :param member_id: 
        """
        self.source_model.removeMemberById(member_id)

    def insertMember(self, member):
        """
        insert member into table
        :param member: 
        """
        self.source_model.insertMember(member)

    def isEmpty(self):
        """
        :return: True if table is empty, False otherwise 
        """
        return self.source_model.rowCount() <= 0

    def getAllData(self):
        """
        :return: all data from table (list of members) 
        """
        return self.source_model._data

    @property
    def name(self):
        return self.lblName.text()

    @property
    def id_(self):
        return self._id_

    @id_.setter
    def id_(self, id_):
        self._id_ = id_
        self.source_model.id_ = id_

    @pyqtSlot()
    def onDeleteClicked(self):
        """
        wrapper for signal for easier binding
        """
        self.deleteClicked.emit(self.id_)

    @pyqtSlot(QPoint)
    def onCustomContextMenuRequested(self, point):
        """
        emit signal with more info for controller to work with (to be able to construct the context menu easier)
        :param point: 
        """
        member = self.table.model().data(self.table.indexAt(point), Qt.UserRole)
        self.contextMenuRequested.emit(member, self.id_, self.table.mapToGlobal(point))
