from PyQt5 import QtCore
from utils.qtmodels.base_table_model import TableModel


class GroupsTableModel(TableModel):
    def __init__(self, groups=(), num_columns=1, parent=None):
        """
        model for groups
        :param groups: list of groups
        :param num_columns: how many columns should the model have (or what it should be saying to it's view)
                should be at least 1 to have data displayed (num_columns=2 leaves 1 empty column)
        :param parent: 
        """
        super(GroupsTableModel, self).__init__(groups, num_columns, parent)

    def headerData(self, p_int, Qt_Orientation, role=None):
        if role == QtCore.Qt.DisplayRole and p_int == 0:
            return 'Název skupiny'