from PyQt5.QtCore import QSortFilterProxyModel
from PyQt5.QtCore import Qt


class CustomFilterModel(QSortFilterProxyModel):
    def __init__(self):
        super(CustomFilterModel, self).__init__()
        self._hidden = []

    def hideRowByData(self, data):
        """
        hide row that represents given Data in model
        :param data
        """
        self._hidden.append(data)
        self.invalidateFilter()

    def resetHiddenRows(self):
        """
        stop hiding any rows (reset to default state, where we're not hiding anything) 
        """
        self._hidden = []
        self.invalidateFilter()

    def filterAcceptsRow(self, row, parent):
        table_data = self.sourceModel().data(self.sourceModel().index(row, 0), Qt.UserRole)
        return super(CustomFilterModel, self).filterAcceptsRow(row, parent) \
               and table_data not in self._hidden
