from PyQt5 import QtCore


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data=[], num_columns=1, parent=None):
        """
        base model that is being inherited
        :param data: 
        :param num_columns: 
        :param parent: 
        """
        super(TableModel, self).__init__(parent)
        self._data = data
        self._columns = num_columns

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._data)

    def columnCount(self, parent=None, *args, **kwargs):
        return self._columns

    def data(self, index, role=None):
        if role == QtCore.Qt.DisplayRole:
            if index.column() == 0:
                return self._data[index.row()].name

        if role == QtCore.Qt.UserRole:
            return self._data[index.row()]

    def getElementById(self, elem_id):
        """
        :param elem_id: id of searched element 
        :return: element with given id, or None if not found
        """
        return next((elem for elem in self._data if elem.id_ == int(elem_id)), None)