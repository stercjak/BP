from PyQt5 import QtCore
from utils.qtmodels.base_table_model import TableModel


class MembersTableModel(TableModel):
    def __init__(self, members=(), num_columns=1, parent=None):
        """
        model for displaying members
        :param members: list of members
        :param num_columns: how many columns should the model have (or what it should be saying to it's view)
                should be at least 1 to have data displayed (num_columns=2 leaves 1 empty column)
        :param parent: 
        """
        super(MembersTableModel, self).__init__(members, num_columns, parent)

    def headerData(self, p_int, Qt_Orientation, role=None):
        if role == QtCore.Qt.DisplayRole:
            if p_int == 0:
                return 'Jméno'
