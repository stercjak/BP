from PyQt5 import QtCore


class HistoryCountTableModel(QtCore.QAbstractTableModel):
    def __init__(self, data={}, num_columns=2, parent=None):
        """
        model for displaying history count of member (how many times he was with other members in a team)
        :param data: dictionary {Member: int, Member: int, ... }
        :param num_columns: how many columns should the model have (or what it should be saying to it's view)
                should be at least 2 to have data displayed (num_columns=3 leaves 1 empty column)
        :param parent: 
        """
        super(HistoryCountTableModel, self).__init__(parent)
        self._data = data
        self._columns = num_columns

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._data)

    def columnCount(self, parent=None, *args, **kwargs):
        return self._columns

    def headerData(self, p_int, Qt_Orientation, role=None):
        if role == QtCore.Qt.DisplayRole:
            if p_int == 0:
                return 'Jméno'
            if p_int == 1:
                return 'Ve stejném týmu'

    def data(self, index, role=None):
        person = list(self._data.keys())[index.row()]
        count = self._data[person]
        if role == QtCore.Qt.DisplayRole:
            if index.column() == 0:
                return person.name
            if index.column() == 1:
                return str(count) + "x"

        if role == QtCore.Qt.UserRole:
            return person

    def getElementById(self, person_id):
        """
        :param elem_id: id of searched member 
        :return: member with given id, or None if not found
        """
        return next((person for person in self._data if person.id_ == int(person_id)), None)
