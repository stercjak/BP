from PyQt5.QtCore import QAbstractItemModel, QModelIndex
from PyQt5.QtCore import Qt


class DistributionTreeModel(QAbstractItemModel):
    def __init__(self, distributions, parent=None):
        """
        tree model for showing distributions of group
        :param distributions: 
        :param parent: 
        """
        super(DistributionTreeModel, self).__init__(parent)
        self._rootNode = self.buildTree(distributions)

    def buildTree(self, distributions):
        """
        build tree from list of distribution 
        (every distribution  is a 1st level node, all of it's teams are it's children and children of teams are members)
        :param distributions: list of distribution
        :return: root node of built tree
        """
        root_node = Node(None, None)
        for dist in distributions:
            dist_node = Node(dist, root_node)
            for team in dist.teams:
                team_node = Node(team, dist_node)
                for member in team.members:
                    member_node = Node(member, team_node)
                    team_node.children.append(member_node)
                dist_node.children.append(team_node)
            root_node.children.append(dist_node)
        return root_node

    def parent(self, index=None):
        node = index.internalPointer()
        parentNode = node.parent
        if parentNode == self._rootNode:
            return QModelIndex()

        if parentNode.parent is not None:
            row = parentNode.parent.children.index(parentNode)
        else:
            row = None

        return self.createIndex(row, 0, parentNode)

    def index(self, row, column, parent):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        childItem = parentNode.children[row]
        return self.createIndex(row, column, childItem)

    def columnCount(self, parent):
        return 1

    def rowCount(self, parent):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return len(parentNode.children)

    def changeData(self, distributions):
        self._rootNode = self.buildTree(distributions)
        self.modelReset.emit()

    def data(self, index, role):
        if not index.isValid():
            return None

        value = index.internalPointer().data

        if role == Qt.DisplayRole:
            return value.name

        if role == Qt.UserRole:
            return value


class Node:
    def __init__(self, data, parent):
        """
        class representing node in tree 
        :param data: 
        :param parent: 
        """
        self.data = data
        self.children = []
        self.parent = parent
