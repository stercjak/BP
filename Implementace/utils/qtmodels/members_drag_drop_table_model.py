from PyQt5.QtCore import Qt, QModelIndex, pyqtSignal

from utils.globals import Global
from utils.qtmodels.members_table_model import MembersTableModel


class MembersDragDropTableModel(MembersTableModel):
    # int1 - id of member dropped here
    # int2 - id of source table (to delete from there)
    droppedMemberFrom = pyqtSignal(int, int)

    def __init__(self, id_, members=[], num_columns=1, parent=None):
        """
        model for displaying members with drag and drop enabled
        :param id_: 
        :param members: 
        :param num_columns: 
        :param parent: 
        """
        super(MembersDragDropTableModel, self).__init__(members, num_columns, parent)
        self.id_ = id_

    def flags(self, index):
        return super(MembersDragDropTableModel, self).flags(index) | Qt.ItemIsDragEnabled | Qt.ItemIsDropEnabled

    def mimeTypes(self):
        return ['application/member.id', 'application/table.id']

    def mimeData(self, indexes):
        mime_data = super(MembersDragDropTableModel, self).mimeData(indexes)
        row = indexes[0].row()
        mime_data.setData('application/member.id', bytes(str(self._data[row].id_), 'ascii'))
        mime_data.setData('application/table.id', bytes(str(self.id_), 'ascii'))
        return mime_data

    def removeRows(self, row, count, parent=None, *args, **kwargs):
        self.beginRemoveRows(QModelIndex(), row, row + count - 1)
        for i in range(count):
            self._data.pop(row)
        self.endRemoveRows()
        return True

    def canDropMimeData(self, data, action, row, column, parent):
        if action == Qt.MoveAction:
            if data.hasFormat('application/member.id') and data.hasFormat('application/table.id'):
                return True
        return False

    def supportedDragActions(self):
        return Qt.MoveAction

    def supportedDropActions(self):
        return Qt.MoveAction

    def dropMimeData(self, data, action, row, column, parent):
        member_id = int(data.data('application/member.id'))
        source_id = int(data.data('application/table.id'))
        self.insertMember(Global.db.getMemberById(member_id))
        self.droppedMemberFrom.emit(member_id, source_id)
        return True

    def insertMember(self, member):
        """
        insert member into table
        :param member: 
        """
        self.beginInsertRows(QModelIndex(), 0, 0)
        self._data.insert(0, member)
        self.endInsertRows()

    def removeMemberById(self, member_id):
        """
        remove member with given id from this table
        :param member_id: id of member that should be removed
        """
        row = None
        for index, member in enumerate(self._data):
            if member.id_ == member_id:
                row = index
                break
        if row is None:
            return
        self.beginRemoveRows(QModelIndex(), row, row)
        self._data.pop(row)
        self.endRemoveRows()
