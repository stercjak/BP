from PyQt5 import QtCore
from PyQt5.QtCore import QModelIndex

from model.requirements import RequirementSameTeam, RequirementDifferentTeam


class RequirementsTableModel(QtCore.QAbstractTableModel):
    def __init__(self, requirements=(), num_columns=3, parent=None):
        """
        model for displaying requirements
        :param requirements: list of requirements
        :param num_columns: how many columns should the model have (or what it should be saying to it's view)
                should be at least 3 to have data displayed (num_columns=4 leaves 1 empty column)
        :param parent: 
        """
        super(RequirementsTableModel, self).__init__(parent)
        self.requirements = list(requirements)
        self._columns = num_columns

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.requirements)

    def columnCount(self, parent=None, *args, **kwargs):
        return self._columns

    def headerData(self, p_int, Qt_Orientation, role=None):
        if role == QtCore.Qt.DisplayRole:
            if role == QtCore.Qt.DisplayRole:
                if p_int == 0:
                    return 'Kdo'
                if p_int == 1:
                    return 'Typ'
                if p_int == 2:
                    return 'S kým'

    def data(self, index, role=None):
        requirement = self.requirements[index.row()]
        if role == QtCore.Qt.DisplayRole:
            if index.column() == 0:
                return  requirement.member.name
            if index.column() == 1:
                return requirement.keyword + ' být v týmu s'
            if index.column() == 2:
                member = requirement.target_members[0].name
                if len(requirement.target_members) > 1:
                    return member + " a " \
                           + str(len(requirement.target_members) - 1) + " dalsi..."
                return member

        if role == QtCore.Qt.UserRole:
            return requirement

    def insertRequirement(self, position, requirement):
        """
        add requirement to table
        :param position: where should be the new requirement placed
        :param requirement: requirement that is to be inserted
        """
        self.beginInsertRows(QModelIndex(), position, position)
        self.requirements.insert(position, requirement)
        self.endInsertRows()

    def getRequirement(self, target_member_id, is_same_team):
        """
        :param target_member_id: id of member that is the 'member' in searched requirement (requirement.member.id)  
        :param is_same_team: True if the type of searched requirement is RequirementSameTeam, False otherwise
        :return: searched requirement, None if not found
        """
        if is_same_team:
            correct_type = RequirementSameTeam
        else:
            correct_type = RequirementDifferentTeam
        return next((requirement for requirement in self.requirements
                     if str(requirement.member.id_) == target_member_id and isinstance(requirement, correct_type)), None)

    def removeRequirement(self, requirement):
        """
        remove requirement from table
        :param requirement: requirement to be removed
        """
        position = self.requirements.index(requirement)
        self.beginRemoveRows(QModelIndex(), position, position)
        self.requirements.remove(requirement)
        self.endRemoveRows()

