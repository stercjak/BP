#-------------------------------------------------
#
# Project created by QtCreator 2017-03-14T13:35:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui \
    groupedit.ui \
    grouptable.ui \
    table.ui \
    distributionsetup.ui \
    requirementdialog.ui \
    distributionoverview.ui \
    memberoverview.ui \
    printwidget.ui \
    editabletitle.ui \
    distribution_setup.ui \
    groupsoverview.ui

SOURCES += \
    ../../controllers/__pycache__/distribution_overview_controller.cpython-36.pyc \
    ../../controllers/__pycache__/distribution_setup_controller.cpython-36.pyc \
    ../../controllers/__pycache__/group_edit_controller.cpython-36.pyc \
    ../../controllers/__pycache__/groups_overview_controller.cpython-36.pyc \
    ../../controllers/__pycache__/mainwindow_controller.cpython-36.pyc \
    ../../controllers/__pycache__/member_overview.cpython-36.pyc \
    ../../controllers/__pycache__/requirement_dialog.cpython-36.pyc \
    ../../model/__pycache__/distribution.cpython-36.pyc \
    ../../model/__pycache__/group.cpython-36.pyc \
    ../../model/__pycache__/member.cpython-36.pyc \
    ../../model/__pycache__/requirements.cpython-36.pyc \
    ../../model/__pycache__/team.cpython-36.pyc \
    ../../utils/__pycache__/distribution_maker.cpython-36.pyc \
    ../../utils/__pycache__/editable_title_widget.cpython-36.pyc \
    ../../utils/__pycache__/groups_table.cpython-36.pyc \
    ../../utils/__pycache__/history_count_table.cpython-36.pyc \
    ../../utils/__pycache__/members_drag_drop_table.cpython-36.pyc \
    ../../utils/__pycache__/members_table.cpython-36.pyc \
    ../../utils/__pycache__/print_widget.cpython-36.pyc \
    ../../utils/__pycache__/requirements_table.cpython-36.pyc \
    ../../utils/__pycache__/table.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/base_table_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/custom_filter_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/distributions_tree_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/groups_table_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/history_count_table_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/members_drag_drop_table_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/members_table_model.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/PersonListModel.cpython-36.pyc \
    ../../utils/qtmodels/__pycache__/requirements_table_model.cpython-36.pyc \
    ../__pycache__/distribution_overview.cpython-36.pyc \
    ../__pycache__/distributionsetup.cpython-36.pyc \
    ../__pycache__/editable_title.cpython-36.pyc \
    ../__pycache__/groupedit.cpython-36.pyc \
    ../__pycache__/groupsoverview.cpython-36.pyc \
    ../__pycache__/mainwindow.cpython-36.pyc \
    ../__pycache__/member_overview.cpython-36.pyc \
    ../__pycache__/persontable.cpython-36.pyc \
    ../__pycache__/print_widget.cpython-36.pyc \
    ../__pycache__/requirement_dialog.cpython-36.pyc \
    ../__pycache__/table.cpython-36.pyc \
    ../../controllers/distribution_overview_controller.py \
    ../../controllers/distribution_setup_controller.py \
    ../../controllers/group_edit_controller.py \
    ../../controllers/groups_overview_controller.py \
    ../../controllers/mainwindow_controller.py \
    ../../controllers/member_overview.py \
    ../../controllers/requirement_dialog.py \
    ../../model/distribution.py \
    ../../model/group.py \
    ../../model/member.py \
    ../../model/requirements.py \
    ../../model/team.py \
    ../../utils/qtmodels/base_table_model.py \
    ../../utils/qtmodels/custom_filter_model.py \
    ../../utils/qtmodels/distributions_tree_model.py \
    ../../utils/qtmodels/groups_table_model.py \
    ../../utils/qtmodels/history_count_table_model.py \
    ../../utils/qtmodels/members_drag_drop_table_model.py \
    ../../utils/qtmodels/members_table_model.py \
    ../../utils/qtmodels/requirements_table_model.py \
    ../../utils/distribution_maker.py \
    ../../utils/editable_title_widget.py \
    ../../utils/groups_table.py \
    ../../utils/history_count_table.py \
    ../../utils/members_drag_drop_table.py \
    ../../utils/members_table.py \
    ../../utils/print_widget.py \
    ../../utils/requirements_table.py \
    ../../utils/table.py \
    ../distribution_overview.py \
    ../distributionsetup.py \
    ../editable_title.py \
    ../groupedit.py \
    ../groupsoverview.py \
    ../mainwindow.py \
    ../member_overview.py \
    ../persontable.py \
    ../print_widget.py \
    ../requirement_dialog.py \
    ../table.py \
    ../../database.py \
    ../../database_create.py \
    ../../globals.py \
    ../../main.py

TRANSLATIONS += ../../translations/czech.ts

RESOURCES += \
    ../../resources/resources.qrc
