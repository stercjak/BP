# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'groupoverview.ui'
#
# Created by: PyQt5 UI code generator 5.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(619, 545)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnCreateGroup = QtWidgets.QPushButton(Form)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/new_group.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnCreateGroup.setIcon(icon)
        self.btnCreateGroup.setObjectName("btnCreateGroup")
        self.horizontalLayout.addWidget(self.btnCreateGroup)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.layoutTables = QtWidgets.QHBoxLayout()
        self.layoutTables.setObjectName("layoutTables")
        self.verticalLayout.addLayout(self.layoutTables)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.btnCreateGroup.setText(_translate("Form", "Vytvořit skupinu"))

