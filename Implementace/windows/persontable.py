# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'persontable.ui'
#
# Created by: PyQt5 UI code generator 5.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tablePeople = QtWidgets.QTableView(Form)
        self.tablePeople.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tablePeople.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tablePeople.setShowGrid(False)
        self.tablePeople.setSortingEnabled(True)
        self.tablePeople.setCornerButtonEnabled(False)
        self.tablePeople.setObjectName("tablePeople")
        self.tablePeople.horizontalHeader().setHighlightSections(False)
        self.tablePeople.verticalHeader().setVisible(False)
        self.tablePeople.verticalHeader().setCascadingSectionResizes(True)
        self.verticalLayout.addWidget(self.tablePeople)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))

