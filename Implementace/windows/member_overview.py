# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'memberoverview.ui'
#
# Created by: PyQt5 UI code generator 5.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(837, 640)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.layoutTitle = QtWidgets.QHBoxLayout()
        self.layoutTitle.setObjectName("layoutTitle")
        spacerItem = QtWidgets.QSpacerItem(0, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layoutTitle.addItem(spacerItem)
        spacerItem1 = QtWidgets.QSpacerItem(30, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.layoutTitle.addItem(spacerItem1)
        self.widgetTitle = QtWidgets.QWidget(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widgetTitle.sizePolicy().hasHeightForWidth())
        self.widgetTitle.setSizePolicy(sizePolicy)
        self.widgetTitle.setObjectName("widgetTitle")
        self.layoutTitle.addWidget(self.widgetTitle)
        spacerItem2 = QtWidgets.QSpacerItem(0, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layoutTitle.addItem(spacerItem2)
        self.verticalLayout.addLayout(self.layoutTitle)
        self.lblGroup = QtWidgets.QLabel(Form)
        self.lblGroup.setAlignment(QtCore.Qt.AlignCenter)
        self.lblGroup.setObjectName("lblGroup")
        self.verticalLayout.addWidget(self.lblGroup)
        self.layoutHistory = QtWidgets.QHBoxLayout()
        self.layoutHistory.setObjectName("layoutHistory")
        spacerItem3 = QtWidgets.QSpacerItem(80, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.layoutHistory.addItem(spacerItem3)
        spacerItem4 = QtWidgets.QSpacerItem(80, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.layoutHistory.addItem(spacerItem4)
        self.verticalLayout.addLayout(self.layoutHistory)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.lblGroup.setText(_translate("Form", "Group"))

