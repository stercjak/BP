# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'requirementdialog.ui'
#
# Created by: PyQt5 UI code generator 5.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(585, 471)
        Dialog.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(10)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.comboWho = QtWidgets.QComboBox(Dialog)
        self.comboWho.setObjectName("comboWho")
        self.horizontalLayout.addWidget(self.comboWho)
        self.comboType = QtWidgets.QComboBox(Dialog)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.comboType.setFont(font)
        self.comboType.setObjectName("comboType")
        self.comboType.addItem("")
        self.comboType.addItem("")
        self.horizontalLayout.addWidget(self.comboType)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setObjectName("widget")
        self.placeTable = QtWidgets.QVBoxLayout(self.widget)
        self.placeTable.setContentsMargins(0, 0, 0, 0)
        self.placeTable.setSpacing(0)
        self.placeTable.setObjectName("placeTable")
        self.verticalLayout.addWidget(self.widget)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Nastavení omezení"))
        self.comboType.setCurrentText(_translate("Dialog", "nesmí"))
        self.comboType.setItemText(0, _translate("Dialog", "nesmí"))
        self.comboType.setItemText(1, _translate("Dialog", "musí"))
        self.label.setText(_translate("Dialog", "být v týmu s:"))

