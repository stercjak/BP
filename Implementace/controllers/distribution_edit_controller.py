from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMessageBox
from controllers.distribution_overview_controller import DistributionOverview
from model.team import Team


class DistributionEdit(DistributionOverview):
    def __init__(self, mainwindow, distribution, group, parent=None):
        """
        :param mainwindow: MainWindow that this belongs to
        :param distribution: Distribution that should be edited
        :param group: Group of the edited distribution
        :param parent: parent widget for qt
        """
        teams = {team.name: team.members for team in distribution.teams}
        super(DistributionEdit, self).__init__(mainwindow, teams, group, distribution.name, parent)
        self.distribution = distribution
        self.distribution_name = distribution.name

    @pyqtSlot()
    def save(self, return_after=True):
        """
        save the edited distribution and return back 
        """
        teams = []
        for table in self.teamTables:
            team = Team(table.id_, table.name, table.getAllData())
            teams.append(team)
        self.distribution.update(self.distribution_name, teams)
        self.main_window.dataChanged()
        self.saved = True
        if return_after is True:
            self.main_window.goBack()

    def returningToPrevious(self):
        """
        give user option to save
        """
        if self.saved is False:
            msg = QMessageBox(self)
            msg.setWindowTitle(self.tr("Uložit?"))
            msg.setText(self.tr("Chcete rozdělení uložit?"))
            msg.setInformativeText(self.tr("Pokud rozdělení neuložíte, všechny provedené změny budou ztraceny."))
            btn_discard = msg.addButton(self.tr("Zahodit změny"), QMessageBox.NoRole)
            btn_save = msg.addButton(self.tr("Uložit"), QMessageBox.YesRole)
            msg.exec_()
            if msg.clickedButton() == btn_save:
                self.save(False)
