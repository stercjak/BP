from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QMessageBox
from controllers.distribution_edit_controller import DistributionEdit
from controllers.distribution_overview_controller import DistributionOverview
from controllers.distribution_setup_controller import DistributionSetup, EmptyGroupError
from controllers.group_edit_controller import GroupEdit
from controllers.groups_overview_controller import GroupsOverview
from controllers.member_overview import MemberOverview
from utils.globals import Global
from windows import mainwindow


class MainWindow(QMainWindow, mainwindow.Ui_MainWindow):
    def __init__(self, parent=None):
        """
        :param parent: parent widget for qt 
        """
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.toolBar.addAction(self.tr("Zpět"), self.goBack).setIcon(QIcon(':/icons/back.svg'))
        self.toolBar.addAction(self.tr("Zpět na přehled skupin"), self.reset).setIcon(QIcon(':/icons/undo.svg'))
        self.goToGroupsOverview()
        self._last_changed = 0

    def _goToWidget(self, widget):
        """
        set current screen to given widget
        :param widget: widget that should be active now
        """
        self.stackedWidget.addWidget(widget)
        self.stackedWidget.setCurrentWidget(widget)

    def dataChanged(self):
        """
        all controllers have to call this when they change data in db
        it prompts every controller before current widget in stack to reload data when getting back to view
        """
        self._last_changed = self.stackedWidget.currentIndex()

    def goToGroupsOverview(self):
        """
        change active window to GroupsOverview()
        """
        self._goToWidget(GroupsOverview(self))

    @pyqtSlot()
    def goToGroupEdit(self, group=None):
        """
        change active window to GroupEdit()
        :param group: if unfilled (or None), create new group
        """
        if group is None:
            self._last_changed = self.stackedWidget.currentIndex() + 1
            group = Global.db.createNewGroup()
        self._goToWidget(GroupEdit(self, group))

    def goToDistributionSetup(self, group):
        """ 
        change active window to DistributionSetup()
        :param group: 
        """
        try:
            self._goToWidget(DistributionSetup(self, group))
        except EmptyGroupError as e:
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle(self.tr("Chyba"))
            msg.setText(self.tr("Skupina nemá žádné členy!"))
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    def goToDistributionOverview(self, teams, group, distribution_name=None):
        """
        change active window to DistributionOverview()
        :param teams: teams in the distribution (dictionary of teams - {"team_name": [Member1, Member2, ...], ... }
        :param group: group from which the members in distribution are
        :param distribution_name: name of distribution to be displayed
        """
        self._goToWidget(DistributionOverview(self, teams, group, distribution_name))

    def goToMemberOverview(self, member):
        """
        change active window to MemberOverview()
        :param member: 
        """
        self._goToWidget(MemberOverview(self, member))

    def goToDistributionEdit(self, distribution, group):
        """
        change active window to DistributionEdit()
        :param distribution: 
        :param group: 
        """
        self._goToWidget(DistributionEdit(self, distribution, group))

    def reset(self):
        """
        Set application to default state (effectively this means emptying 'back' stack and going to GroupsOverview()
        """
        for i in range(self.stackedWidget.count() - 1, -1, -1):
            widget = self.stackedWidget.widget(i)
            widget.returningToPrevious()
            self.stackedWidget.removeWidget(widget)
            widget.deleteLater()
        self._last_changed = 0
        self.goToGroupsOverview()

    def goBack(self):
        """
        go one step back, to previous screen
        """
        current_index = self.stackedWidget.currentIndex()
        current_widget = self.stackedWidget.currentWidget()
        if current_index <= 0:
            # we're on groups overview (do nothing)
            return
        current_widget.returningToPrevious()
        if current_index <= self._last_changed:
            self.stackedWidget.widget(current_index - 1).reloadData()
            self._last_changed -= 1
        self.stackedWidget.removeWidget(current_widget)
        current_widget.deleteLater()
        self.stackedWidget.setCurrentIndex(current_index - 1)
