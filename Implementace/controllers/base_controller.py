
class BaseController:
    def reloadData(self):
        """
        dummy implementation of reloadData, each child can reimplement it 
        (to prevent showing data, that are different from data in db)  
        """
        pass

    def returningToPrevious(self):
        """
        dummy implementation of returningToPrevious, each child can reimplement it
        (it's opportunity to do something when leaving the screen - going back or going to start of app)
        """
        pass
