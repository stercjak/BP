from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget, QMessageBox

from controllers.base_controller import BaseController
from utils.widgets.history_count_table import HistoryCountTable
from utils.database import IntegrityError
from utils.widgets.editable_title_widget import EditableTitle
from windows import member_overview


class MemberOverview(BaseController, QWidget, member_overview.Ui_Form):
    def __init__(self, mainwindow, member, parent=None):
        """
        :param mainwindow: MainWindow that this belongs to 
        :param member: member that will be displayed
        :param parent: parent widget for qt
        """
        super(MemberOverview, self).__init__(parent)
        self.setupUi(self)

        self.main_window = mainwindow

        self.member = member
        self.lblGroup.setText(self.member.group_name)

        title = EditableTitle(self.member.name, self)
        self.layoutTitle.replaceWidget(self.widgetTitle, title)
        self.widgetTitle = title

        self.tableCount = HistoryCountTable(data=member.history_count, parent=self)
        self.layoutHistory.insertWidget(1, self.tableCount)

        self.widgetTitle.textConfirmed.connect(self.changeName)


    @pyqtSlot(str)
    def changeName(self, name):
        """
        change members name
        :param name: 
        """
        try:
            self.member.rename(name)
            self.main_window.dataChanged()
        except IntegrityError as e:
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Critical)
            msg.setText(self.tr("Ve skupině {0} již existuje člen s tímto jménem.").format(self.member.group_name))
            msg.setWindowTitle(self.tr("Nelze přejmenovat člena"))
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec()
            self.widgetTitle.editMode()

