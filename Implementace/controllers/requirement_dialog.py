from PyQt5 import Qt
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QDialog, QAbstractItemView, QLabel
from model.requirements import RequirementDifferentTeam, RequirementSameTeam
from utils.widgets.members_table import MembersTable
from windows import requirement_dialog


class NewRequirementDialog(QDialog, requirement_dialog.Ui_Dialog):
    def __init__(self, group, all_requirements, parent=None):
        """
        :param group: group for which we're adding requirement 
        :param all_requirements: all other requirements that are already created for this distribution
                (to prevent creating multiple requirements for the same person)
        :param parent: parent widget for qt
        """
        super(NewRequirementDialog, self).__init__(parent)
        self.setupUi(self)
        self.group = group
        self.requirements = all_requirements

        self.tableMembers = MembersTable(data=self.group.members, parent=self)
        self.placeTable.insertWidget(0, self.tableMembers)
        self.tableMembers.table.setSelectionMode(QAbstractItemView.MultiSelection)

        # set up comboWho
        for member in sorted(self.group.members, key=lambda member: member.name):
            if len([req for req in all_requirements if req.member == member]) < 2:
                self.comboWho.addItem(member.name, member)
        self.comboWho.setCurrentIndex(0)
        self.comboWho.currentIndexChanged.connect(self.onComboWhoCurrentChanged)
        self.onComboWhoCurrentChanged()  # to set initial filter

    @pyqtSlot()
    def onComboWhoCurrentChanged(self):
        """
        update members that are shown in the table (able to be selected)
        """
        self.tableMembers.table.clearSelection()
        self.tableMembers.model.resetHiddenRows()
        self.tableMembers.model.hideRowByData(self.comboWho.currentData(Qt.UserRole))
        self.comboType.clear()

        # find what requirement with this person exists (to disable adding the same type)
        cur_req = next((requirement for requirement in self.requirements
                        if requirement.member == self.comboWho.currentData(Qt.UserRole)), None)
        if not isinstance(cur_req, RequirementSameTeam):
            self.comboType.addItem("musí", RequirementSameTeam)
        if not isinstance(cur_req, RequirementDifferentTeam):
            self.comboType.addItem("nesmí", RequirementDifferentTeam)

    @property
    def requirement(self):
        """
        construct Requirement from what is filled in dialog
        :return: Requirement()
        """
        target_members = self.tableMembers.getSelectedItems()
        if len(target_members) == 0:
            return None
        who = self.comboWho.currentData(Qt.UserRole)
        return self.comboType.currentData(Qt.UserRole)(who, target_members)


class RequirementEditDialog(QDialog, requirement_dialog.Ui_Dialog):
    def __init__(self, group, requirement, parent=None):
        """
        :param group: group for which we're adding requirement 
        :param requirement: requirement that is being edited
        :param parent: parent widget for qt
        """
        super(RequirementEditDialog, self).__init__(parent)
        self.setupUi(self)
        self.group = group
        self._requirement = requirement
        self.comboWho.hide()
        self.comboType.hide()

        # set up labels
        self.horizontalLayout.replaceWidget(self.comboWho, QLabel(requirement.member.name))
        self.horizontalLayout.replaceWidget(self.comboType, QLabel("<b>" + requirement.keyword + "</b>"))
        # set up table
        self.tableMembers = MembersTable(data=self.group.members, parent=self)
        self.placeTable.insertWidget(0, self.tableMembers)
        self.tableMembers.table.setSelectionMode(QAbstractItemView.MultiSelection)
        self.tableMembers.model.hideRowByData(requirement.member)
        model = self.tableMembers.model
        for i in range(model.rowCount()):
            if model.data(model.index(i, 0), Qt.UserRole) in requirement.target_members:
                self.tableMembers.table.selectRow(i)
        self.tableMembers.table.setFocus()


    @property
    def requirement(self):
        """
        update requirement with the data filled in dialog
        :return: updated requirement 
        """
        target_people = self.tableMembers.getSelectedItems()
        if len(target_people) == 0:
            return None
        self._requirement.target_members = target_people
        return self._requirement
