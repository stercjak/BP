from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from utils.widgets.members_table import MembersTable

from controllers.base_controller import BaseController
from model.group import Group
from model.member import Member
from utils.globals import Global
from utils.widgets.groups_table import GroupsTable
from windows import groupsoverview


class GroupsOverview(BaseController, QWidget, groupsoverview.Ui_Form):
    def __init__(self, main_window, parent=None):
        """
        :param main_window: MainWindow that this belongs to 
        :param parent: parent widget for qt
        """
        super(GroupsOverview, self).__init__(parent)
        self.main_window = main_window
        self.setupUi(self)
        self.btnCreateGroup.clicked.connect(main_window.goToGroupEdit)

        # add groups table
        self.groups_table = GroupsTable(
            num_of_buttons=2,
            button_definitions=({'text': "", 'tooltip': self.tr("Rozdělit"), 'icon': ":/icons/split.svg"},
                                {'text': "", 'tooltip': self.tr("Přehled"), 'icon': ":/icons/eye.svg"}),
            data=Global.db.getGroups(),
            parent=self
        )
        self.layoutTables.addWidget(self.groups_table)
        self.groups_table.btnClicked.connect(self.onGroupBtnClicked)
        self.groups_table.itemDoubleClicked.connect(self.editGroup)
        self.groups_table.elementSelected.connect(self.showMembers)

        # add members table
        self.members_table = MembersTable(
            num_of_buttons=1,
            button_definitions=({'text': "", 'tooltip': self.tr("Přehled"), 'icon': ":/icons/eye.svg"},),
            parent=self
        )
        self.layoutTables.addWidget(self.members_table)
        self.members_table.btnClicked.connect(self.viewMember)
        self.members_table.itemDoubleClicked.connect(self.viewMember)

    def reloadData(self):
        """
        reload data from db
        """
        self.groups_table.changeData(Global.db.getGroups())
        self.members_table.changeData([])

    @pyqtSlot(Group)
    def showMembers(self, group):
        """
        change content of members table to correspond with groups table selection
        :param group: 
        """
        self.members_table.changeData(group.members)

    @pyqtSlot(Group, int)
    def onGroupBtnClicked(self, group, btn_id):
        """
        slot for tableGroups, when button is clicked
        :param group: 
        :param btn_id: 
        """
        if btn_id == 0:
            self.distribute(group)
        if btn_id == 1:
            self.editGroup(group)

    @pyqtSlot(Group)
    def editGroup(self, group):
        """
        go to GroupEdit for given group 
        :param group: 
        """
        self.main_window.goToGroupEdit(group)

    @pyqtSlot(Member)
    def viewMember(self, member):
        """
        go to MemberOverview for given member
        :param member: 
        """
        self.main_window.goToMemberOverview(member)

    def distribute(self, group):
        """
        go to DistributionSetup for given group
        :param group: 
        """
        self.main_window.goToDistributionSetup(group)
