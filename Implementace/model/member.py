from utils.globals import Global


class Member:
    def __init__(self, id_, name, group_id):
        """
        :param id_: id of member 
        :param name: name of member
        :param group_id: id of group this member belongs to
        """
        self.id_ = id_
        self.name = name
        self.group_id = group_id
        self._history_count = None

    @property
    def group_name(self):
        """
        :return: name of group this member belongs to 
        """
        return Global.db.getGroupName(self.group_id)

    @property
    def history_count(self):
        """
        :return: dictionary of members with count how many times this member was in the same team with them
                {Member1: 0, Member2: 1, Member3: 0, ...}
        """
        if self._history_count is None:
            self._history_count = Global.db.getHistoryCount(self.id_)
        return self._history_count

    def delete(self):
        """
        delete this member from database completely (also from every distribution he was in)
        """
        Global.db.deleteMember(self.id_)

    def rename(self, new_name):
        """
        rename this member
        :param new_name: 
        :raise database.IntegrityError if the name is not unique in member's group
        """
        if new_name == self.name:
            return
        Global.db.changeMemberName(self.id_, new_name)
        self.name = new_name

    def __eq__(self, other):
        if not isinstance(other, Member):
            return False
        return self.id_ == other.id_

    def __hash__(self):
        return self.id_
