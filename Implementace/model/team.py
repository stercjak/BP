

class Team:
    def __init__(self, id_, name, members):
        """
        :param id_: id of team 
        :param name: name of team
        :param members: list of members of team
        """
        self.id_ = id_
        self.name = name
        self.members = members
