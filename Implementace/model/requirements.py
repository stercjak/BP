

class Requirement:
    def __init__(self, member, target_members, keyword):
        """
        :param member: the one member that's in some "relationship" with each of the target_members 
        :param target_members: see member
        :param keyword: the word that represents this requirement ("member -keyword- in the same team as...")
        """
        self.member = member
        self.target_members = target_members
        self.keyword = keyword


class RequirementSameTeam(Requirement):
    def __init__(self, member, target_members):
        """
        requirement representing requirement type 'must be in the same team'
        """
        super(RequirementSameTeam, self).__init__(member, target_members, "musí")


class RequirementDifferentTeam(Requirement):
    def __init__(self, member, target_members):
        """
        requirement representing requirement type 'must be in the different team'
        """
        super(RequirementDifferentTeam, self).__init__(member, target_members, "nesmí")
