from utils.globals import Global


class Group:
    NEW_GROUP = -1

    def __init__(self, id_, name=''):
        """
        :param id_: id of group if id_ = -1 new group will be created 
        :param name: name of group
        """
        if id == self.NEW_GROUP:
            self.id_, self.name = Global.db.createNewGroup()
        else:
            self.id_ = id_
            self.name = name
        self._members = None

    @property
    def members(self):
        """
        :return: list of members in group 
        """
        if self._members is None:
            self._members = Global.db.getGroupMembers(self.id_)
        return self._members

    @property
    def member_count(self):
        """
        :return: count of members 
        """
        if self._members is None:
            self.members  # to force initialization
        return len(self._members)

    @property
    def distributions(self):
        """
        :return: list of distribution of this group 
        """
        return Global.db.getDistributionsByGroupID(self.id_)

    def delete(self):
        """
        delete group from db
        """
        Global.db.deleteGroup(self.id_)

    def rename(self, new_name):
        """
        change name of this group
        :param new_name: 
        :raise database.IntegrityError if the name is not unique
        """
        if new_name == self.name:
            return
        Global.db.changeGroupName(self.id_, new_name)
        self.name = new_name

    def addMember(self, name):
        """
        Create and add new member to group
        :param name: 
        :raise database.IntegrityError if the name is not unique in this group
        """
        # force initialization of members
        self.members
        self._members.append(Global.db.createMember(self.id_, name))

