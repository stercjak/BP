from utils.globals import Global


class Distribution:
    def __init__(self, name, teams, group_id=None, id_=None):
        """
        :param name: name of group 
        :param teams: list of teams in distribution
        :param group_id: id of group that is distributed
        :param id_: id of distribution
        """
        self.id_ = id_
        self.name = name
        self.teams = teams
        self.group_id = group_id

    def saveToDb(self):
        """
        save distribution to db
        """
        self.id_ = Global.db.createDistribution(self.name, self.group_id)
        curs = Global.db.beginTransaction()
        for team in self.teams:
            team_id = Global.db.createTeam(team.name, self.id_, curs=curs)
            Global.db.addMembersToTeam(team_id, team.members, curs=curs)
        Global.db.endTransaction(curs)

    def update(self, new_name, new_teams):
        """
        update this distribution, actually this is done by deleting self from database and then reinserting
        :param new_name: 
        :param new_teams: 
        """
        self.deleteFromDb()
        self.name = new_name
        self.teams = new_teams
        self.saveToDb()

    def deleteFromDb(self):
        """
        delete this distribution from database
        """
        Global.db.deleteDistribution(self.id_)