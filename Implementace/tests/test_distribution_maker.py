import unittest
import os
import sqlite3
from controllers.distribution_setup_controller import Violated
from model.requirements import RequirementSameTeam, RequirementDifferentTeam
from utils.database import Database
from utils.globals import Global
from utils.distribution_maker import DistributionMaker


class TestDistributionMaker(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        script_path = os.path.dirname(os.path.abspath(__file__))
        db_path = script_path + "/test.db"
        try:
            os.remove(db_path)
        except FileNotFoundError:
            pass
        connection = sqlite3.connect(db_path)
        with open(script_path + "/test_db_create.sql") as create_script:
            connection.executescript(create_script.read())
        Global.db = Database(db_path)

    def checkDistributionValid(self, members, teams, violated_requirements, requirements, team_count, min_team_size,
                               max_team_size):
        # check if distribution contains each member
        for member in members:
            for team in teams:
                if member in team:
                    break
            else:
                self.fail("Distribution doesn't contain all members")
        # check that requirements are either fulfilled or disclaimed as violated in violated_requirements
        for requirement in requirements:
            if requirement not in violated_requirements:
                member_team = next((team for team in teams if requirement.member in team), None)
                for target_member in requirement.target_members:
                    if isinstance(requirement, RequirementSameTeam):
                        self.assertIn(target_member, member_team,
                                      requirement.member.name + " is not in the same team with " + target_member.name
                                      + " and the distribution doesn't disclaim it in getViolatedRequirements()")
                    if isinstance(requirement, RequirementDifferentTeam):
                        self.assertNotIn(target_member, member_team,
                                      requirement.member.name + " is in the same team with " + target_member.name
                                      + " and the distribution doesn't disclaim it in getViolatedRequirements()")

        # for each of team_count, min_team_size and max_team_size check if fulfilled or disclaimed as violated
        if team_count is not None:
            if Violated.TEAM_COUNT not in violated_requirements:
                self.assertEqual(len(teams), team_count, "Distribution doesn't have " + str(team_count)
                                 + " teams and Violated.TEAM_COUNT is not in getViolatedRequirements()")
                for team in teams:
                    self.assertGreaterEqual(len(team), 1, "Distribution has " + str(team_count)
                                            + " but at least one of those teams is empty")

        if min_team_size is not None:
            if Violated.MIN_TEAM_SIZE not in violated_requirements:
                for team in teams:
                    self.assertGreaterEqual(len(team), min_team_size,
                                            "Distribution has smaller team than " + str(min_team_size)
                                            + " and Violated.MIN_TEAM_SIZE is not in getViolatedRequirements()")

        if max_team_size is not None:
            if Violated.MAX_TEAM_SIZE not in violated_requirements:
                for team in teams:
                    self.assertLessEqual(len(team), max_team_size,
                                         "Distribution has larger team than " + str(max_team_size)
                                         + " and Violated.MAX_TEAM_SIZE is not in getViolatedRequirements()")

    def test_no_requirements(self):
        members = Global.db.getGroupMembers(3)
        dist_maker = DistributionMaker(members, [], Violated, None, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], None, None, None)

    def test_30_to_5_teams(self):
        members = Global.db.getGroupMembers(3)
        dist_maker = DistributionMaker(members, [], Violated, 5, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, None)

    def test_30_to_5_teams_min_6(self):
        members = Global.db.getGroupMembers(3)
        dist_maker = DistributionMaker(members, [], Violated, 5, 6, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, 6, None)

    def test_30_to_5_teams_max_6(self):
        members = Global.db.getGroupMembers(3)
        dist_maker = DistributionMaker(members, [], Violated, 5, None, 6, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, 6)

    def test_30_to_5_teams_prefer_same(self):
        members = Global.db.getGroupMembers(3)
        dist_maker = DistributionMaker(members, [], Violated, 5, None, None, DistributionMaker.PREFER_SAME_TEAMS)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, None)

    def test_30_to_5_teams_prefer_different(self):
        members = Global.db.getGroupMembers(3)
        dist_maker = DistributionMaker(members, [], Violated, 5, None, None, DistributionMaker.PREFER_DIFFERENT_TEAMS)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, None)

    def test_30_to_5_teams_same_team_possible(self):
        members = Global.db.getGroupMembers(3)
        requirement = RequirementSameTeam(members[0], [members[1], members[2], members[3], members[4], members[5], ])
        dist_maker = DistributionMaker(members, [requirement], Violated, 5, None, 6, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, 6)

    def test_30_to_5_teams_same_team_impossible(self):
        members = Global.db.getGroupMembers(3)
        requirement = RequirementSameTeam(
            members[0],
            [members[1], members[2], members[3], members[4], members[5], members[6], members[7], ]
        )
        dist_maker = DistributionMaker(members, [requirement], Violated, 5, None, 6, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, 6)

    def test_30_to_5_teams_different_team_possible(self):
        members = Global.db.getGroupMembers(3)
        requirement = RequirementDifferentTeam(
            members[0],
            [members[1], members[2], members[3], members[4], members[5], members[6], members[7], ]
        )
        dist_maker = DistributionMaker(members, [requirement], Violated, 5, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, None)

    def test_30_to_5_teams_different_team_possible_prefer_same(self):
        members = Global.db.getGroupMembers(3)
        requirement = RequirementDifferentTeam(
            members[0],
            [members[1], members[2], members[3], members[4], members[5], members[6], members[7], ]
        )
        dist_maker = DistributionMaker(members, [requirement], Violated, 5, None, None, DistributionMaker.PREFER_SAME_TEAMS)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], 5, None, None)

    def test_5_different_team_possible(self):
        members = Global.db.getGroupMembers(2)
        requirement = RequirementDifferentTeam(
            members[0],
            [members[1], ]
        )
        dist_maker = DistributionMaker(members, [requirement], Violated, None, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], None, None, None)

    def test_5_different_team_impossible(self):
        members = Global.db.getGroupMembers(2)
        requirement = RequirementDifferentTeam(
            members[0],
            [members[1], members[2], members[3], members[4], ]
        )
        dist_maker = DistributionMaker(members, [requirement], Violated, None, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], None, None, None)

    def test_5_different_team_same_team_possible(self):
        members = Global.db.getGroupMembers(2)
        requirement1 = RequirementSameTeam(
            members[0],
            [members[1], members[2], ]
        )
        requirement2 = RequirementDifferentTeam(
            members[0],
            [members[3], ]
        )
        dist_maker = DistributionMaker(members, [requirement1, requirement2], Violated, None, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], None, None, None)

    def test_5_different_team_same_team_impossible(self):
        members = Global.db.getGroupMembers(2)
        requirement1 = RequirementSameTeam(
            members[0],
            [members[1], members[2], ]
        )
        requirement2 = RequirementDifferentTeam(
            members[1],
            [members[0], ]
        )
        dist_maker = DistributionMaker(members, [requirement1, requirement2], Violated, None, None, None, None)
        teams = dist_maker.distribute()
        violated_requirements = dist_maker.getViolatedRequirements()
        self.checkDistributionValid(members, teams, violated_requirements, [], None, None, None)
